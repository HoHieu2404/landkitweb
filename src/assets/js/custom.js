
window.onload = function () {

    // --------- Open/Close menu  ---------
    const btnOpen = document.getElementById('button-open');
    const btnClose = document.getElementById('button-close');
    const menuRow = document.getElementsByClassName("dropdown-menu__row")

    let swapCardState = 0
    let state = 0

    btnOpen.addEventListener('click', function onClick(event) {
        btnOpen.style.display = 'none';
        body.classList.add('show');

        for (let index = 0; index < menuRow.length; index++) {
            menuRow[index].addEventListener('click', function onClick(event) {
                if (state = !state)
                    document.getElementsByClassName("dropdown-menu__row__sub")[index].style.display = "block";
                else document.getElementsByClassName("dropdown-menu__row__sub")[index].style.display = "none";
            });
        }
    });
    btnClose.addEventListener('click', function onClick(event) {
        body.classList.remove("show")


    });

    // -------------- Slider  -------------
    const btnSwapCard1 = document.getElementById("btn-swap-card-1")
    const btnSwapCard2 = document.getElementById("btn-swap-card-2")
    const cardAirbnb = document.getElementById("card-airbnb")
    const text = document.getElementsByClassName("relative")

    const transitionState = ["-100%", "0", "100%", "200%"]
    btnSwapCard1.addEventListener('click', function onClick(event) {
        transitionState.push(transitionState.shift())
        for (let i = 0; i < text.length; i++) {
            text[i].style.left = transitionState[i]
            if (transitionState[i] === "0")
                text[i].style.zIndex = "4"
            else text[i].style.zIndex = "-10"

        }
        if (swapCardState = !swapCardState) {

            cardAirbnb.style.opacity = "0";
        }
        else {
            cardAirbnb.style.opacity = "1";

        }

    });
    btnSwapCard2.addEventListener('click', function onClick(event) {
        transitionState.unshift(transitionState.pop())
        for (let i = 0; i < text.length; i++) {
            text[i].style.left = transitionState[i]
            if (transitionState[i] === "0")
                text[i].style.zIndex = "4"
            else text[i].style.zIndex = "-10"

        }
        if (swapCardState = !swapCardState)
            cardAirbnb.style.opacity = "0";
        else
            cardAirbnb.style.opacity = "1";
    });


    // ---------- Validate form ----------

    const btnDownload = document.getElementById("button-download")
    const inputName = document.getElementById("inputName")
    const inputEmail = document.getElementById("inputEmail")
    const inputPassword = document.getElementById("inputPassword")
    const txtError = document.getElementsByClassName("text-error")
    const inputTitle = document.getElementsByClassName("input-title")
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    btnDownload.addEventListener('click', function onClick(event) {
        validateForm()
    })

    inputName.addEventListener("focusout", function onFocusout(event) {
        if (inputName.value !== "") inputTitle[0].classList.add("move")
        else inputTitle[0].classList.remove("move")
    });
    inputName.addEventListener("keydown", function onFocusout(event) {
        if (event.key === "Enter") validateForm()
    });

    inputEmail.addEventListener("focusout", function onFocusout(event) {
        if (inputEmail.value !== "") inputTitle[1].classList.add("move")
        else inputTitle[1].classList.remove("move")
    });
    inputEmail.addEventListener("keydown", function onFocusout(event) {
        if (event.key === "Enter") validateForm()
    });

    inputPassword.addEventListener("focusout", function onFocusout(event) {
        if (inputPassword.value !== "") inputTitle[2].classList.add("move")
        else inputTitle[2].classList.remove("move")
    });
    inputPassword.addEventListener("keydown", function onFocusout(event) {
        if (event.key === "Enter") validateForm()
    });

    function validateForm() {
        let validateState = 0
        if (inputName.value === "") {
            txtError[0].style.display = "block"
            validateState = 1
        }
        else {
            txtError[0].style.display = "none"
            validateState = 0
        }

        if (!filter.test(inputEmail.value)) {
            txtError[1].style.display = "block"
            validateState = 0
        }
        else {
            txtError[1].style.display = "none"
            validateState = 1
        }

        if (inputPassword.value.length < 8) {
            txtError[2].style.display = "block"
            validateState = 0
        }
        else {
            txtError[2].style.display = "none"
            validateState = 1
        }
        const result = "name: " + inputName.value + ", \nemail: " + inputEmail.value + ", \npassword: " + inputPassword.value;
        if (validateState) console.log(result)
    }



    
// ------------------------------------------------- Animation -------------------------------------------------
  
    // ----- Animation section About -----
    const typedTextSpan = document.getElementById("typed-text")
    const textArray = ["developers.", "founders.", "designers."]
    const typingDelay = 50;
    const erasingDelay = 50;
    const newTextDelay = 100;
    let textArrayIndex = 0;
    let charIndex = 0;
    function type() {
      if (charIndex < textArray[textArrayIndex].length) {
        typedTextSpan.innerHTML += textArray[textArrayIndex].charAt(charIndex)
        charIndex++;
        setTimeout(type, typingDelay);
      } else {
        setTimeout(erase, newTextDelay + 1100);
      }
    }
    function erase() {
      if (charIndex > 0) {
        typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex - 1);
        charIndex--;
        setTimeout(erase, erasingDelay);
      } else {
        textArrayIndex++;
        if (textArrayIndex >= textArray.length) textArrayIndex = 0;
        setTimeout(type, typingDelay);
      }
    }
    setTimeout(type, newTextDelay + 250);
  
  
    // ------ Animation section Stats ------
    const txtSatisfaction = document.getElementById("txtSatisfaction")
    const txtSupportTime = document.getElementById("txtSupportTime")
    const txtSupportDate = document.getElementById("txtSupportDate")
    const txtSales = document.getElementById("txtSales")
    animationNumber(txtSatisfaction, 0, 100, 500, "%")
    animationNumber(txtSupportTime, 1, 24, 500, "/")
    animationNumber(txtSupportDate, 1, 7, 500, "")
    animationNumber(txtSales, 1, 100, 500, "k+")
    
  
  
    // ----- Animation section Pricing -----
    const txtPerSeat = document.getElementById("txtPerSeat")
    const checkBox = document.getElementById("checkBox")
    const checkBoxButton = document.getElementById("checkBoxButton")
    let checkBoxState = 0
    checkBox.addEventListener('click', function onClick(event) {
      if(checkBoxState = !checkBoxState){
        animationNumber(txtPerSeat, 29, 49, 500,"")
        document.getElementsByClassName("content__paragraph__check-box")[0].style.backgroundColor = "#335eea"
        checkBoxButton.style.left = "45%"
      }
      else  {
        animationNumber(txtPerSeat, 49, 29, 500,"")
        document.getElementsByClassName("content__paragraph__check-box")[0].style.backgroundColor = "#d9e2ef"
        checkBoxButton.style.left = "0"
      }
      console.log("sdsds");
  
    })
    function animationNumber(obj, start, end, duration, text) {
      let startTimestamp = null;
      const step = (timestamp) => {
        if (!startTimestamp) startTimestamp = timestamp;
        const progress = Math.min((timestamp - startTimestamp) / duration, 1);
        obj.innerHTML = Math.floor(progress * (end - start) + start) + text;
        if (progress < 1) {
          window.requestAnimationFrame(step);
        }
      };
      window.requestAnimationFrame(step);
    }

}