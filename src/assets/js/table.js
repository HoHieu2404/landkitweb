var dataSet = [
    ['Tiger Nixon', 'System Architect', 'Edinburgh', '5421', '2011/04/25', '$320,800'],
    ['Garrett Winters', 'Accountant', 'Tokyo', '8422', '2011/07/25', '$170,750'],
    ['Ashton Cox', 'Junior Technical Author', 'San Francisco', '1562', '2009/01/12', '$86,000'],
    ['Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', '6224', '2012/03/29', '$433,060'],
    ['Airi Satou', 'Accountant', 'Tokyo', '5407', '2008/11/28', '$162,700'],
    ['Brielle Williamson', 'Integration Specialist', 'New York', '4804', '2012/12/02', '$372,000'],
    ['Herrod Chandler', 'Sales Assistant', 'San Francisco', '9608', '2012/08/06', '$137,500'],
    ['Rhona Davidson', 'Integration Specialist', 'Tokyo', '6200', '2010/10/14', '$327,900'],
    ['Colleen Hurst', 'Javascript Developer', 'San Francisco', '2360', '2009/09/15', '$205,500'],
    ['Sonya Frost', 'Software Engineer', 'Edinburgh', '1667', '2008/12/13', '$103,600'],
    ['Jena Gaines', 'Office Manager', 'London', '3814', '2008/12/19', '$90,560'],
    ['Quinn Flynn', 'Support Lead', 'Edinburgh', '9497', '2013/03/03', '$342,000'],
    ['Charde Marshall', 'Regional Director', 'San Francisco', '6741', '2008/10/16', '$470,600'],
    ['Haley Kennedy', 'Senior Marketing Designer', 'London', '3597', '2012/12/18', '$313,500'],
    ['Tatyana Fitzpatrick', 'Regional Director', 'London', '1965', '2010/03/17', '$385,750'],
    ['Michael Silva', 'Marketing Designer', 'London', '1581', '2012/11/27', '$198,500'],
    ['Paul Byrd', 'Chief Financial Officer (CFO)', 'New York', '3059', '2010/06/09', '$725,000'],
    ['Gloria Little', 'Systems Administrator', 'New York', '1721', '2009/04/10', '$237,500'],
    ['Bradley Greer', 'Software Engineer', 'London', '2558', '2012/10/13', '$132,000'],
    ['Dai Rios', 'Personnel Lead', 'Edinburgh', '2290', '2012/09/26', '$217,500'],
    ['Jenette Caldwell', 'Development Lead', 'New York', '1937', '2011/09/03', '$345,000'],
    ['Yuri Berry', 'Chief Marketing Officer (CMO)', 'New York', '6154', '2009/06/25', '$675,000'],
    ['Caesar Vance', 'Pre-Sales Support', 'New York', '8330', '2011/12/12', '$106,450'],
    ['Doris Wilder', 'Sales Assistant', 'Sydney', '3023', '2010/09/20', '$85,600'],
    ['Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', '5797', '2009/10/09', '$1,200,000'],
    ['Gavin Joyce', 'Developer', 'Edinburgh', '8822', '2010/12/22', '$92,575'],
    ['Jennifer Chang', 'Regional Director', 'Singapore', '9239', '2010/11/14', '$357,650'],
    ['Brenden Wagner', 'Software Engineer', 'San Francisco', '1314', '2011/06/07', '$206,850'],
    ['Fiona Green', 'Chief Operating Officer (COO)', 'San Francisco', '2947', '2010/03/11', '$850,000'],
    ['Shou Itou', 'Regional Marketing', 'Tokyo', '8899', '2011/08/14', '$163,000'],
    ['Michelle House', 'Integration Specialist', 'Sydney', '2769', '2011/06/02', '$95,400'],
    ['Suki Burks', 'Developer', 'London', '6832', '2009/10/22', '$114,500'],
    ['Prescott Bartlett', 'Technical Author', 'London', '3606', '2011/05/07', '$145,000'],
    ['Gavin Cortez', 'Team Leader', 'San Francisco', '2860', '2008/10/26', '$235,500'],
    ['Martena Mccray', 'Post-Sales support', 'Edinburgh', '8240', '2011/03/09', '$324,050'],
    ['Unity Butler', 'Marketing Designer', 'San Francisco', '5384', '2009/12/09', '$85,675'],
];
let keys = ["name", "position", "office", "extn", "startDate", "salarys"];
let dataObjects = dataSet.map(r => (keys.reduce((o, k, i) => (o[k] = r[i], o), {})));
let currentData = dataObjects
const entriesRecord = 10
let numOfRecord = entriesRecord
let numOfPage = 0


function pagination(index) {
    numOfPage = index;
    loadData(currentData, numOfRecord * index, numOfRecord * index + numOfRecord, index)
}
window.onload = function () {
    const recordSelectButton = document.getElementById("recordSelectButton")
    const table = document.getElementById("table")
    const listButton = document.getElementById("listButton")
    const name = document.getElementById("name")
    const position = document.getElementById("position")
    const office = document.getElementById("office")
    const extn = document.getElementById("extn")
    const startDate = document.getElementById("startDate")
    const salarys = document.getElementById("salarys")
    const inputSearch = document.getElementById("inputSearch")
    const iconUp = document.getElementsByClassName("icon-up")
    const iconDown = document.getElementsByClassName("icon-down")
    const txtStartRecord = document.getElementById("txtStartRecord")
    const txtEndRecord = document.getElementById("txtEndRecord")
    const txtTotalOfRecord = document.getElementById("txtTotalOfRecord")
    const txtFilter = document.getElementById("txtFilter")
    const btnPrevious = document.getElementById("btnPrevious")
    const btnNext = document.getElementById("btnNext")


    let sortState = {
        name: 0,
        position: 0,
        office: 0,
        extn: 0,
        startDate: 0,
        salarys: 0,
    }

    let startRecord = 0
    let endRecord = entriesRecord

    loadData(currentData, startRecord, endRecord, numOfPage)
    txtTotalOfRecord.innerHTML = dataObjects.length


    // ---------------------------- change page  --------------------------
    btnPrevious.addEventListener('click', function handleChange(e) {
        pagination(--numOfPage)
        console.log(numOfPage);
    });
    btnNext.addEventListener('click', function handleChange(e) {
        pagination(++numOfPage)
        console.log(numOfPage);

    });


    // ------------------------ select record number  ----------------------
    recordSelectButton.addEventListener('change', function handleChange(e) {
        startRecord = 0
        endRecord = parseInt(e.target.value)
        numOfRecord = parseInt(e.target.value)
        loadData(currentData, startRecord, endRecord, numOfPage)
    });


    // ------------------------------- search -------------------------------
    inputSearch.addEventListener('keyup', function onChange(e) {
        currentData = dataObjects
        const inputString = e.target.value
        let data = []
        currentData.find((obj, index) => {
            for (var value in obj) {
                if (currentData[index][value].toLowerCase().indexOf(inputString) + 1) {
                    data.push(currentData[index])
                    break;
                }
            }
        });
        if (e.target.value !== "") txtFilter.innerHTML = `(filtered from ${dataObjects.length} total entries)`
        else txtFilter.innerHTML = ""
        txtTotalOfRecord.innerHTML = data.length
        currentData = data
        startRecord = 0
        endRecord = numOfRecord
        loadData(currentData, startRecord, endRecord, numOfPage)
    });


    // -------------------------------- sort --------------------------------
    name.addEventListener('click', function handleChange(e) {
        if (sortState["name"] = !sortState["name"]) {
            currentData = currentData.sort(dynamicSort("name", sortState["name"]))
            iconUp[0].style.color = "#111"
            iconDown[0].style.color = "#ccc"
        }
        else {
            currentData = currentData.sort(dynamicSort("name", sortState["name"]))
            iconUp[0].style.color = "#ccc"
            iconDown[0].style.color = "#111"
        }
        startRecord = 0
        loadData(currentData, startRecord, endRecord, numOfPage)
    });

    position.addEventListener('click', function onClick(e) {
        if (sortState["position"] = !sortState["position"]) {
            currentData = currentData.sort(dynamicSort("position", sortState["position"]))
            iconUp[1].style.color = "#111"
            iconDown[1].style.color = "#ccc"
        }
        else {
            currentData = currentData.sort(dynamicSort("position", sortState["position"]))
            iconUp[1].style.color = "#ccc"
            iconDown[1].style.color = "#111"
        }
        startRecord = 0
        loadData(currentData, startRecord, endRecord, numOfPage)
    });

    office.addEventListener('click', function onClick(e) {
        if (sortState["office"] = !sortState["office"]) {
            currentData = currentData.sort(dynamicSort("office", sortState["office"]))
            iconUp[2].style.color = "#111"
            iconDown[2].style.color = "#ccc"
        }
        else {
            currentData = currentData.sort(dynamicSort("office", sortState["office"]))
            iconUp[2].style.color = "#ccc"
            iconDown[2].style.color = "#111"
        }
        startRecord = 0
        loadData(currentData, startRecord, endRecord, numOfPage)
    });

    extn.addEventListener('click', function onClick(e) {
        if (sortState["extn"] = !sortState["extn"]) {
            currentData = currentData.sort(dynamicSort("extn", sortState["extn"]))
            iconUp[3].style.color = "#111"
            iconDown[3].style.color = "#ccc"
        }
        else {
            currentData = currentData.sort(dynamicSort("extn", sortState["extn"]))
            iconUp[3].style.color = "#ccc"
            iconDown[3].style.color = "#111"
        }
        startRecord = 0
        loadData(currentData, startRecord, endRecord, numOfPage)
    });

    startDate.addEventListener('click', function onClick(e) {
        if (sortState["startDate"] = !sortState["startDate"]) {
            currentData = currentData.sort(dynamicSort("startDate", sortState["startDate"]))
            iconUp[4].style.color = "#111"
            iconDown[4].style.color = "#ccc"
        }
        else {
            currentData = currentData.sort(dynamicSort("startDate", sortState["startDate"]))
            iconUp[4].style.color = "#ccc"
            iconDown[4].style.color = "#111"
        }
        startRecord = 0
        loadData(currentData, startRecord, endRecord, numOfPage)
    });

    salarys.addEventListener('click', function onClick(e) {
        if (sortState["salarys"] = !sortState["salarys"]) {
            currentData = currentData.sort(dynamicSort("salarys", sortState["salarys"]))
            iconUp[5].style.color = "#111"
            iconDown[5].style.color = "#ccc"
        }
        else {
            currentData = currentData.sort(dynamicSort("salarys", sortState["salarys"]))
            iconUp[5].style.color = "#ccc"
            iconDown[5].style.color = "#111"
        }
        startRecord = 0
        loadData(currentData, startRecord, endRecord, numOfPage)
    });

}

function loadData(data, start, end, numOfPage) {
    if (data.length > 0) {
        let row = ""
        for (let index = start; index < end; index++) {
            let rowItem = "";
            for (var value in data[index]) {
                if (data[index].hasOwnProperty(value)) {
                    rowItem += `<td>${data[index][value]}</td>`
                }
            }
            row += `<tr class="row-item">${rowItem}</tr>`
        }
        table.innerHTML = row
        txtStartRecord.innerHTML = start + 1
        txtEndRecord.innerHTML = end <= currentData.length ? end : currentData.length

        btnPrevious.disabled = false
        btnNext.disabled = false
        if (data.length <= numOfRecord) {
            btnPrevious.disabled = true
            btnNext.disabled = true
        }
        else {
            if (numOfPage === 0) btnPrevious.disabled = true
            else if (numOfPage >= parseInt(data.length / numOfRecord)) btnNext.disabled = true
        }

        let div = ""
        for (let index = 0; index < currentData.length / numOfRecord; index++) {
            div += numOfPage != index
                ? `<button class="button-pagination button--black" onclick="pagination(${index})">${index + 1}</button>`
                : `<button class="button-pagination button--black button-active" onclick="pagination(${index})">${index + 1}</button>`
        }
        listButton.innerHTML = div
    }
    else {
        table.innerHTML = '<tr class="row-item"><td style="width: 880px;text-align: center;">No matching records found</td></tr>'
        listButton.innerHTML = ""
        txtStartRecord.innerHTML = 0
        txtEndRecord.innerHTML = 0
        btnPrevious.disabled = true
        btnNext.disabled = true
    }
}



function dynamicSort(property, sortState) {
    if (sortState) {
        return function (a, b) {
            let result
            if (property === "salarys") {
                let x = parseInt(a[property].split(/[$,,,]/).join(''))
                let y = parseInt(b[property].split(/[$,,,]/).join(''))
                result = (x < y) ? -1 : (x > y) ? 1 : 0;
            }
            else result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result;
        }
    }
    return function (a, b) {
        let result
        if (property === "salarys") {
            let x = parseInt(a[property].split(/[$,,,]/).join(''))
            let y = parseInt(b[property].split(/[$,,,]/).join(''))
            result = (x > y) ? -1 : (x < y) ? 1 : 0;
        }
        else result = (a[property] > b[property]) ? -1 : (a[property] < b[property]) ? 1 : 0;
        return result;
    }
}

